import React from 'react'
import Layout from "../Layout/Layout";
import Banner from '../Banner/Banner';

const Home = () => {
  return (
    <Layout>
      <Banner />
    </Layout>
  )
}

export default Home
